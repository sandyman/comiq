<?php

require_once dirname(__FILE__) . "/../vendor/autoload.php";

use PostgresDBAL\DatabaseRawConnection;

class Database
{
    static public function connect($dbname, $user, $password='', $host='')
    {
        $connection_string = 'dbname=' . $dbname;
        $connection_string .= ' user=' . $user;
        $connection_string .= ' password=' . (strlen($password) > 0 ? $password : '');
        if (strlen($host) > 0)
        {
            $connection_string .= ' host=' . $host;
        }

        return new DatabaseRawConnection($connection_string);
    }
}

?>