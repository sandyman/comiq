<?php

/**
 * Class CreateClassFile
 */
class CreateClassFile
{
	/**
	 * This pattern matches the table name (\1) and the fields of the table (\2)
	 */
	const PATTERN_TABLE = '/CREATE\sTABLE\s(\w+)\s*\((.*)\);/sU';

	/**
	 * This pattern matches the fields of the table using their name (\1) and their type (\2)
	 */
	const PATTERN_FIELDS = '/^([a-zA-Z0-9_]+)\s+(\w+).*$/';

	/**
	 * This map converts SQL keywords to PHP keywords. Can be augmented with additional types.
	 *
	 * @var array
	 */
	private $keyword_translation = [
		'bigint' => 'int',
		'integer' => 'int',
		'int' => 'int',
		'text' => 'string',
		'timestamp' => 'string',
		'varchar' => 'string',
		'real' => 'float',
		'float' => 'float',
		'bool' => 'boolean',
		'boolean' => 'boolean',
	];

	/**
	 * @var bool
	 */
	private $create_getters = true;

	/**
	 * @var bool
	 */
	private $create_setters = false;

	/**
	 * @var bool
	 */
	private $create_private_constructor = false;

	/**
	 * @var string
	 */
	private $filename = '';

	/**
	 * @var string
	 */
	private $tablename = '';

	/**
	 * @var int
	 */
	private $num_fields = 0;

	/**
	 * @var array
	 */
	private $fields = [];

	/**
	 * @var array
	 */
	private $types = [];

	/**
	 * @var bool
	 */
	private $to_stdout = true;

	/**
	 * @var string Actual output string
	 */
	private $class_string = '';

	/**
	 * Public constructor
	 *
	 * @param array $options
	 *
	 * The following options are supported:
	 *
	 * -f <filename>  input file
	 * -g             create getters [default]
	 * -G             don't create getters
	 * -o             output to file [default: stdout]. Output filename is <ClassName>.php.
	 * -p             create public constructor [default]
	 * -P             create private constructor
	 * -s             create setters
	 * -S             don't create setters [default]
	 */
	public function __construct($options)
	{
		if (! array_key_exists('f', $options))
		{
			echo "You must provide an input file using the mandatory option -f <filename>.\n";
			exit(-3);
		}

		$this->filename = $options['f'];

		/* output option, to file or to stdout */
		if (array_key_exists('o', $options))
		{
			$this->to_stdout = false;
		}

		/*
		 * Options for getters, setters and constructor. If both are provided, e.g., -Gg,
		 * we fall back to the [default].
		 */
		if (array_key_exists('G', $options))
		{
			$this->create_getters = false;
		}
		if (array_key_exists('g', $options))
		{
			/* [default] */
			$this->create_getters = true;
		}

		if (array_key_exists('P', $options))
		{
			$this->create_private_constructor = true;
		}
		if (array_key_exists('p', $options))
		{
			/* [default] */
			$this->create_private_constructor = false;
		}

		if (array_key_exists('s', $options))
		{
			$this->create_setters = true;
		}
		if (array_key_exists('S', $options))
		{
			/* [default] */
			$this->create_setters = false;
		}
	}

	/**
	 * Run the script
	 */
	public function run()
	{
		if (strlen($this->filename) === 0)
		{
			echo "Please provide a filename.\n";
			exit;
		}

		$file = @file_get_contents($this->filename);
		$matches = [];
		if (1 === preg_match(self::PATTERN_TABLE, $file, $matches))
		{
			$this->tablename = $matches[1];
			$fields = explode("\n", $matches[2]);
			foreach ($fields as $field)
			{
				$matches = [];
				$field = trim($field);
				if (1 === preg_match(self::PATTERN_FIELDS, $field, $matches))
				{
					$name = $matches[1];	/* SQL field name */
					$type = $matches[2];	/* SQL field type */

					$this->num_fields++;
					$this->fields[]= $name;
					$ltype = strtolower($type);
					if (array_key_exists($ltype, $this->keyword_translation))
					{
						$this->types[] = $this->keyword_translation[$ltype];
					}
					else
					{
						throw new RuntimeException("Unknown field type: " . $type);
					}
				}
			}

			if ($this->num_fields > 0)
			{
				$this->create_class();
			}
			else
			{
				echo "No fields found! Is this a valid SQL file?\n";
			}
		}
		else
		{
			echo "Table does not match!\n";
		}
	}

	/**
	 * Create class
	 */
	private function create_class()
	{
		/* E.g., board_item[.sql] => BoardItem */
		$class_name = implode('', array_map('ucfirst', explode('_', $this->tablename)));

		/* Create header for the php file */
		$this->create_header($class_name);

		/* Create members */
		$this->create_members();

		/* Create getters */
		if ($this->create_getters)
		{
			$this->create_getters();
		}

		/* Create getters */
		if ($this->create_setters)
		{
			$this->create_setters();
		}

		/* Create public constructor */
		$this->create_constructor();

		/* Create footer for the php file */
		$this->create_footer();

		/* Echo to stdout or to file, depending on flag -o */
		if ($this->to_stdout)
		{
			echo $this->class_string;
		}
		else
		{
			/* ClassName is filename */
			$filename = "{$class_name}.php";
			file_put_contents($filename, $this->class_string, FILE_TEXT);
		}
	}

	/**
	 * Create header for class file.
	 *
	 * @param string $class_name
	 */
	private function create_header($class_name)
	{
		/* Create header of file */
		$this->class_string = <<< S_CLASS
<?php

class {$class_name}
{
    //----------------------------------------------------------------------- Constants

    const TABLENAME = '{$this->tablename}';

S_CLASS;
	}

	/**
	 * Create footer for class file.
	 */
	private function create_footer()
	{
		/* Finally: create footer of file */
		$this->class_string .= <<< E_CLASS

    /* EOAC: END OF AUTOGENERATED CODE */

};

?>
E_CLASS;
	}

	/**
	 * Create member variables
	 */
	private function create_members()
	{
		$this->class_string .= "\n";
		$this->class_string .= "    //----------------------------------------------------------------------- Members\n\n";
		for ($i = 0; $i < $this->num_fields; $i++)
		{
			$fieldname = $this->fields[$i];
			$fieldtype = $this->types[$i];

			$this->class_string .= <<<FIELD
    /**
     * @var $fieldtype	
     */
    private \${$fieldname};
FIELD;

			$this->class_string .= "\n\n";
		}
	}

	/**
	 * Create getters
	 */
	private function create_getters()
	{
		$this->class_string .= "    //----------------------------------------------------------------------- Getters\n\n";
		for ($i = 0; $i < $this->num_fields; $i++)
		{
			$fieldname = $this->fields[$i];
			$fieldtype = $this->types[$i];

			$matches = [];
			$methodname = $fieldname;
			$pattern = "/{$this->tablename}_(.*)/";
			if (1 === preg_match($pattern, $fieldname, $matches))
			{
				$methodname = $matches[1];
			}
			$cast = (0 === strcmp('int', $fieldtype)) ? '(int)' : '';

			$this->class_string .= <<< METHOD
    /**
     * @return {$fieldtype}
     */
    public function {$methodname}()
    {
        return {$cast}\$this->{$fieldname};
    }
METHOD;
			$this->class_string .= "\n\n";
		}
	}

	/**
	 * Create setters
	 */
	private function create_setters()
	{
		$this->class_string .= "    //----------------------------------------------------------------------- Setters\n\n";
		for ($i = 0; $i < $this->num_fields; $i++)
		{
			$fieldname = $this->fields[$i];
			$fieldtype = $this->types[$i];

			$matches = [];
			$methodname = $fieldname;
			$pattern = "/{$this->tablename}_(.*)/";
			if (1 === preg_match($pattern, $fieldname, $matches))
			{
				$methodname = $matches[1];
			}
			$cast = (0 === strcmp('int', $fieldtype)) ? '(int)' : '';

			$this->class_string .= <<< METHOD
    /**
     * @param {$fieldtype} \${$fieldname}
     */
    public function set_{$methodname}(\${$fieldname})
    {
        \$this->{$fieldname} = {$cast}\${$fieldname};
    }
METHOD;
			$this->class_string .= "\n\n";
		}
	}

	/**
	 * Create constructor. Depending on the create_private_constructor flag, this constructor
	 * will be public (false) or private (true).
	 */
	private function create_constructor()
	{
		$accessibility = ($this->create_private_constructor) ? "private" : "public";

		$this->class_string .= <<< CONSTRUCTOR
    //----------------------------------------------------------------------- Constructor

    /**
     * @var array \$record
    */
    {$accessibility} function __construct(\$record)
    {
        assert({$this->num_fields} === count(\$record));
CONSTRUCTOR;
		$this->class_string .= "\n\n";

		for ($i = 0; $i < $this->num_fields; $i++)
		{
			$fieldname = $this->fields[$i];
			$fieldtype = $this->types[$i];

			$cast = (0 === strcmp('int', $fieldtype)) ? '(int)' : '';
			$this->class_string .= "        \$this->{$fieldname} = {$cast}\$record['{$fieldname}'];\n";
		}
		$this->class_string .= "    }\n";
	}
};


/**
* Run as script, only when called by its name
*/
if (basename(__FILE__) === basename($argv[0]))
{
	$options = getopt("f:gGopPsS");
	$parser = new CreateClassFile($options);
	$parser->run();
}

?>