<?php

require_once dirname(__FILE__) . "/Database.php";

use PostgresDBAL\DatabaseConnection;

/**
 * Class Comiq
 */
class Comiq
{
    // ------------------------------------------------------------------------
    // Static Methods

    /**
     * @var DatabaseConnection $singletonConnection
     */
    private static $singletonConnection = null;

    /**
     * @return \PostgresDBAL\DatabaseConnection
     */
    public static function GetDatabaseConnection()
    {
        // Lazy create the singleton connection
        if (is_null(self::$singletonConnection))
        {
            self::$singletonConnection = Database::connect('comiq', 'postgres', '', 'localhost');
        }

        // Return the singleton connection
        return self::$singletonConnection;
    }
}

?>