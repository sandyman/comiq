DROP TABLE person;

DROP SEQUENCE person_id;

CREATE SEQUENCE person_id;

CREATE TABLE person
(
  person_id		      	        integer Primary Key DEFAULT nextval('person_id'),
  person_timestamp			      timestamp with time zone NOT NULL DEFAULT now(),

  person_name                 text,
  person_first_name           text,
  person_last_name            text
);
