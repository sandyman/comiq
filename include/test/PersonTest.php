<?php

require_once dirname(__FILE__) . "/../setup.php";

require_once dirname(__FILE__) . "/../Comiq.php";
require_once dirname(__FILE__) . "/../Person.php";


/**
 * Class PersonTest
 */
class PersonTest extends PHPUnit_Framework_TestCase
{
    static private $connection;

    /**
     *
     */
    public static function setUpBeforeClass()
    {
        self::$connection = Comiq::GetDatabaseConnection();
    }

    /**
     *
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     *
     */
    public function setUp()
    {
    }

    /**
     *
     */
    public function tearDown()
    {
    }

    /**
     * Test whether the class is instantiated correctly
     */
    public function test_instantiate()
    {
        $record = [
            'person_id' => 1,
            'person_timestamp' => 'now()',
            'person_name' => 'Test User',
            'person_first_name' => 'Test',
            'person_last_name' => 'User'
        ];
        $person = new Person($record);

        $this->assertInstanceOf('Person', $person, "Variable \$person is not an instance of Person");

        $this->assertEquals(1, $person->id(), "Person's id should be 1");

        $this->assertTrue(strlen($person->timestamp()) > 0, "Timestamp should have a value");

        $this->assertEquals($person->name(), 'Test User', "Person's name should be Test User");

        $this->assertEquals($person->first_name(), 'Test', "Person's first name should be Test");

        $this->assertEquals($person->last_name(), 'User', "Person's last name should be User");
    }
}

?>